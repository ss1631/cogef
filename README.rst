COnstrained Geometries simulate External Force
==============================================

COGEF is an ASE module which contains tools for simulating force-induced
bond-breaking reactions based on the COnstrained Geometries simulate External
Force (COGEF) method (Beyer, M. K. J. Chem. Phys. 2000, 112, 7307).

Website: https://cogef.gitlab.io/cogef


Requirements
------------

* Python_ 3.7
* NumPy_ 1.16
* ASE_ 3.18


Installation
------------

* Installation with pip::

  $ sudo pip install --upgrade pip
  $ # If you want to install ase with pip and the latest ase pypi release is
  $ # still equal to 3.17, install the newest ase version with the following
  $ # command
  $ pip install git+https://gitlab.com/ase/ase.git
  $ # In all cases you can install cogef with the next command
  $ pip install ase-cogef

* Developer installation with git (when there is no need for merge requests)

Clone the repository::

  $ git clone git@gitlab.com:cogef/cogef.git

Add ``cogef`` folder to the $PYTHONPATH environment variable.
Add ``cogef/bin`` folder to the $PATH environment variable.

* Developer installation with git (when you may want to create merge requests)

Go to https://gitlab.com/cogef/cogef and fork the project, then clone it
with your gitlab account name::

  $ git clone git@gitlab.com:your-user-name/cogef.git

Add ``cogef`` folder to the $PYTHONPATH environment variable.
Add ``cogef/bin`` folder to the $PATH environment variable.

Testing
-------

Please run the tests::

  $ cogef test


Contact
-------

* Functional Nanosystems group::

  https://www.functional-nanosystems.uni-freiburg.de/People/PDWalter

* Oliver Brügner::

  oliver.bruegner@fit.uni-freiburg.de


Example
-------

See https://cogef.gitlab.io/cogef/tutorials/cogef.


.. _Python: http://www.python.org/
.. _ASE: http://wiki.fysik.dtu.dk/ase
.. _NumPy: http://docs.scipy.org/doc/numpy/reference/
