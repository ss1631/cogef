.. module:: cogef

=========
Tutorials
=========

We want to simulate external forces by the appication of 
geometry constraints. This "COnstraint GEometry to simulate Forces"
is known as the COGEF method [Beyer2000]_.

1S-COGEF
========

The standard COGEF method constrains two atoms usually by their distance 
and relaxes all other degrees of freedom. 

We first relax a linear chain of atoms with EMT::

  from ase.atoms import Atoms
  from ase.optimize import FIRE
  from ase.calculators.emt import EMT

  fmax = 0.01

  image = Atoms('AuAgAg', positions=((-1, 0, 0), (0, 0, 0), (1, 0, 0)))
  image.set_calculator(EMT())
  FIRE(image).run(fmax=fmax)

Now we pull on the ends (atom indices 0, 1) of the chain::

  from cogef import COGEF
  
  
  def initialize(image, imagenum, new_opt, get_filename):
      """Initialize the image and return the trajectory name.

      """
      if get_filename:
          return 'cogef' + str(imagenum) + '.traj'
      image.set_calculator(EMT())

  cogef = COGEF([image], 0, 2, optimizer=FIRE, fmax=fmax)
  stepsize = 0.1
  steps = 30
  cogef.pull(stepsize, steps, initialize, 'cogef.traj')

which writes the file ``cogef.traj`` containing the corresponding trajectory.
Observing the trajectory shows that th Ag-Ag bond breaks. The maximum
force needed for separation can be either obtained from a 
numerical derivative of the energy or directly from the forces::

  from ase.units import J, m

  f = m / J * 1e9  # eV / A -> nN

  print('Maximal force: {0:4.2f} nN (from energies),'.format(
      cogef.get_maximum_force(method='use_energies') * f) +
      ' {0:4.2f} nN (from forces)'.format(
          cogef.get_maximum_force(method='use_forces') * f))

We may to include finite temperature into consideration. This is done through
the ``Dissociation`` object that allows to obtain force-dependent 
propability distributions  :math:`dp/dF`

.. image:: 1scogef.png

The code used::

  from cogef import Dissociation
  import pylab as plt

  LOADING_RATE = 10  # [nN/s]
  T = 300      # Temperature [K]
  P = 101325.  # Pressure    [Pa]

  plt.figure()
  diss = Dissociation(cogef,
                      force_unit='nN')  # To have loading rate in nN/s and
                                        # all forces in nN in the input and
                                        # output of Dissociation methods
  fstep = 0.003
  for T in [5, 50, 100, 200, 300, 500]:
      if 0:
          # Manual limits
          fmin, fmax = 0.2, 2
          DPDF, F = diss.probability_density(
              T, P, LOADING_RATE, fmax, fmin, fstep, method='electronic')
          p = plt.plot(F, DPDF, '-', label=None)
          color = p[0].get_color()
      else:
          color = None
      # Automatic limits
      fmin, fmax = diss.get_force_limits(T, P, LOADING_RATE, force_step=fstep,
                                         method='electronic')
      DPDF, F = diss.probability_density(
          T, P, LOADING_RATE, fmax, fmin, fstep, method='electronic')
      p = plt.plot(F, DPDF, 'o-', color=color,
                   label='T={0}K'.format(T))
      plt.axvline(x=cogef.get_maximum_force(method='use_energies') * f,
                  ls='--', color='k')

  plt.legend(loc=2)
  plt.ylim(0, 110)
  plt.xlabel('F [nN]')
  plt.ylabel('dp/dF [nN$^{-1}$]')
  plt.show()

The peaks define the average rupture force and the standard deviation::

  for T in [5, 50, 100, 200, 300, 500]:
      fmin, fmax = diss.get_force_limits(T, P, LOADING_RATE, force_step=fstep,
                                         method='electronic')
      force, error = diss.rupture_force_and_uncertainty(
          T, P, LOADING_RATE, fmax, fmin, fstep, method='electronic')
      print('Rupture force ({0}K): ({1:4.2f} +- {2:4.2f}) nN'.format(
          T, force, error))

Rupture force calculation can be performed automatically, that is the number
of needed steps for molecule stretching is obtained. The loop in the next
example stops when there are enough images in order to obtain the
rupture force::

  T = 300
  fmax = 0.01
  stepsize = 0.1
  max_steps = 100

  image = Atoms('AuAgAg', positions=((-1, 0, 0), (0, 0, 0), (1, 0, 0)))
  image.set_calculator(EMT())
  FIRE(image).run(fmax=fmax)

  cogef = COGEF([image], 0, 2, optimizer=FIRE, fmax=fmax)
  diss = Dissociation(cogef, force_unit='nN')
  for step in range(max_steps + 1):
      try:
          fmin, fmax = diss.get_force_limits(T, P, LOADING_RATE,
                                             force_step=fstep,
                                             method='electronic')
          break
      except ValueError as msg:
          if diss.error not in [1, 2, 3]:
              raise ValueError(msg)
          assert step < max_steps, 'Step limit reached.'
          # Add one image
          cogef.pull(stepsize, 1, initialize, 'cogef.traj')
  force, error = diss.rupture_force_and_uncertainty(
      T, P, LOADING_RATE, fmax, fmin, fstep, method='electronic')
  print('Rupture force: ({0:4.3f} +- {1:4.3f}) nN'.format(force, error))

Gibbs energies can be used instead of electronic energies by a vibrational
analysis and a calculation based on ``IdealGasThermo``. The difference
in rupture force is very small for this example::

  def initialize_vib(image, dirname):
      """Initialize the image for vibrational analysis.

      """
      image.set_calculator(EMT())


  diss = Dissociation(cogef, initialize_vib, vib_method='frederiksen',
                      force_unit='nN')
  diss.geometry = 'linear'  # Like in class IdealGasThermo

  fmin, fmax = diss.get_force_limits(T, P, LOADING_RATE, force_step=fstep,
                                     method='Gibbs')
  force, error = diss.rupture_force_and_uncertainty(
      T, P, LOADING_RATE, fmax, fmin, fstep, method='Gibbs')
  print('Rupture force: ({0:4.3f} +- {1:4.3f}) nN'.format(force, error))

3S-COGEF
========

.. (We proposed the 3S-COGEF method that allows also to break the AuAg bond.)

The transition state on the 1S-COGEF path can be wrong for long molecules as
indicated by a discontinuity in energy [Brügner2018]_.

.. image:: 1scogef_problem.png

This picture was obtained for a linear molecule out of ten atoms connected
by Morse potentials. We let external forces act on indices 0 and 8 instead of
0 and 9 in order to break the symmetry and to break only one bond::

  import pylab as plt

  from ase.atoms import Atoms
  from ase.optimize import FIRE
  from ase.calculators.morse import MorsePotential
  from cogef import COGEF

  fmax = 0.05

  image = Atoms('H10', positions=[(i, 0, 0) for i in range(10)])
  image.set_calculator(MorsePotential())
  FIRE(image).run(fmax=fmax)


  def initialize(image, imagenum, new_opt, get_filename):
      """Initialize the image and return the trajectory name.

      """
      if get_filename:
          return 'cogef1s' + str(imagenum) + '.traj'
      image.set_calculator(MorsePotential())


  cogef = COGEF([image], 0, 8, optimizer=FIRE, fmax=fmax)
  stepsize = 0.03
  steps = 35

  cogef.pull(stepsize, steps, initialize, 'cogef.traj')
  # Get the 1S-COGEF path
  energies, distances = cogef.get_energy_curve()
  plt.figure(0)
  plt.plot(distances, energies)
  plt.xlabel('d [$\\AA$]')
  plt.ylabel('U [eV]')
  plt.savefig('1scogef_problem.png')

Let us obtain electronic activation energies and rate constants from 1S-COGEF
for a later comparison with 3S-COGEF::

  T = 300      # Temperature [K]
  P = 101325.  # Pressure    [Pa]
  force_min = 1.
  force_max = 4.
  force_step = 0.1

  diss = Dissociation(cogef, force_unit='nN')
  rates_1scogef, forces_1scogef = diss.get_rate_constants(
      T, P, force_max, force_min, force_step, method='electronic')
  barriers_1scogef = []
  for f_ext in forces_1scogef:
      barriers_1scogef.append(diss.electronic_energy_barrier(f_ext))

The 3S-COGEF method is based on a three-segmented path through a
two-dimenisonal energy landscape. It can be used to get exacter transition
states, activation energies and rate constants for the dissociation reaction
in dependence of the external force. The following picture shows that
1S-COGEF overestimates the energy barrier and the error decreases with force
compared to 3S-COGEF.

.. image:: 1scogef_error.png

The class ``COGEF2D`` was used to apply the 3S-COGEF method. The first segment
of the 3S-COGEF path is the 1S-COGEF path up to bond breaking.
This path can also be obtained with COGEF2D, but we will
import it as we have it already. The breaking bond with length *b* must be set
([0, 1]) in addition to the end-to-end distance *d* defined like before.
Finally we choose between two different methods for variation of *b*.
Here, we want to fix the external force and not *d* which is often simpler to
use for chain molecules::

  from cogef import COGEF2D, Dissociation2d
  from os.path import join


  def initialize2d(image, directory, imagenum, new_opt, get_filename):
      """Initialize the image or return the trajectory name.

      """
      if initialize(image, -1, new_opt, get_filename) == 'Finished':
          return 'Finished'
      if get_filename:
          return join(directory, 'cogef2d') + str(imagenum) + '.traj'


  cogef = COGEF2D([0, 8], [0, 1], 'cogef1s.traj', optimizer=FIRE, fmax=fmax,
                  fix_force_for_max_curve=True)

The discontinuity was already identified and we now the last image number,
where the configuration of the 1S-COGEF path has an intact bond. This should
be set to prevent error messages in the following::

  cogef.set_last_intact_bond_image(26)

The second and the third segments of the 3S-COGEF method (maximum and minimum
with respect to variation of *b*) should be obtained as far as we
need them to obtain rate constants in the force range defined earlier::

  energy_tolerance = 0.01
  diss = Dissociation2d(cogef, force_unit='nN',
                        transition_into_two_fragments=True)


  def get_rates():
      # Use some global variables
      while True:
          try:
              rates = diss.get_rate_constants(
                  T, P, force_max, force_min, force_step, method='electronic')
              break
          except ValueError:
              assert diss.error in [1, 2]
              # More images must be calculated
              if len(diss.needed_max_images) > 0:
                  I = diss.needed_max_images[-1]
                  diss.clean_needed_images_list()
                  is_max = True
              elif len(diss.needed_min_images) > 0:
                  I = diss.needed_min_images[-1]
                  diss.clean_needed_images_list()
                  is_max = False
              else:
                  I = None
              if (I is None or len(cogef.images) <= I):
                  # Calculate 1S-COGEF path further
                  cogef.pull(stepsize, 1, initialize, 'cogef1s.traj')
              else:
                  if is_max:
                      cogef.calc_maximum_curve(
                          [I], stepsize, initialize2d=initialize2d,
                          max_trajectory='maximum.traj',
                          breakdirectory='pull',
                          min_trajectory='minimum.traj',
                          only_minimum_curve=not is_max,
                          energy_tolerance=energy_tolerance)
                  else:
                      cogef.calc_maximum_curve(
                          [I], stepsize, initialize2d=initialize2d,
                          max_trajectory='maximum.traj',
                          breakdirectory='pull',
                          min_trajectory='minimum.traj',
                          and_minimum_curve=not is_max,
                          energy_tolerance=energy_tolerance)
      return rates


  rates_3scogef, forces_3scogef = get_rates()

This will return an error message:
"ValueError: Cannot find energy maximum. Maximum image number is too small or
energy_tolerance is too large or too small. If you want to increase
energy_tolerance, remove pull_2/pull.traj first."
Look into pull_2/pull.traj and you will see that the energy maximum cannot
be identified within 0.01 eV energy tolerance. We do not want to decrease this
tolerance, so let the program allow to calculate more images in
*b*-direction::

  cogef.max_image_number = 30
  rates_3scogef, forces_3scogef = get_rates()
  barriers_3scogef = []
    for f_ext in forces_3scogef:
        barriers_3scogef.append(diss.electronic_energy_barrier(f_ext))

Now we can obtain the picture from above for comparison of 1S- and 3S-COGEF::

  plt.figure(1)
  plt.plot(forces_1scogef, rates_1scogef, '--', color='blue', label='1S-COGEF',
           lw=2)
  plt.plot(forces_3scogef, rates_3scogef, color='blue', label='3S-COGEF', lw=2)
  plt.xlabel('F [nN]')
  plt.ylabel('Rate constant k [1/s]', color='blue')
  plt.yscale('log')
  ax1 = plt.gca()
  for tl in ax1.get_yticklabels():
      tl.set_color('blue')
  plt.legend(loc=5)

  ax2 = plt.twinx()
  ax2.plot(forces_1scogef, barriers_1scogef, '--', color='green', lw=2)
  ax2.plot(forces_3scogef, barriers_3scogef, color='green', lw=2)
  try:
      ddagger = unichr(int('2021', 16))  # Python 2
  except NameError:
      ddagger = chr(int('2021', 16))  # Python 3
  plt.ylabel('Activation energy $\\Delta U^' + ddagger + '$ [eV]',
             color='green')
  for tl in ax2.get_yticklabels():
      tl.set_color('green')

  plt.xlim(1, 4)
  plt.savefig('1scogef_error.png')

Rate constants can be saved with::

  diss.save_rate_constants(
      T, P, force_max, force_min, force_step, method='electronic',
      fileout='bond1.dat')

Your python file for saving rate constants, and the trajectory file of the
1S-COGEF path can be copied in new directories to obtain rate constants for
breaking of other bonds of the same molecule. Only the definition of *b*
must be changed. We can obtain rupture forces by taking multiple dissociation
reactions into account. In the following we take only two::

  from numpy import array
  from cogef import load_rate_constants, Minima, probability_density
  from cogef import rupture_force_and_uncertainty_from_dpdf

  loading_rate = 1000.  # nN/s as defined in the Dissociation2d objects
                        # used for saving rate constants and associated forces

  rates_bond1, forces_bond1 = load_rate_constants('bond1.dat')
  rates_bond2, forces_bond2 = load_rate_constants('bond2.dat')
  forces = forces_bond1
  assert forces == forces_bond2

  minima = Minima()
  minima.add_destination(rates_bond1)
  minima.add_destination(rates_bond2)
  dpdf = probability_density(minima, forces, loading_rate)
  print('Final probabilities:')
  print(minima.prob)
  f_rup, f_err = rupture_force_and_uncertainty_from_dpdf(
      -array(dpdf[0]), forces)
  print('Rupture force:')
  print(str(f_rup) + 'nN')
  print('Standard deviation:')
  print(str(f_err) + 'nN')

In order to be sure that the force range is large enough, it is helpful to
plot the peak in ``-dpdf[0]`` over ``forces`` and check whether the final
probability of the reactant state ``minima.prob[0]`` is almost zero.

References
==========

.. [Beyer2000] Beyer, M. K. J. Chem. Phys. **112** (2000) 7307
.. [Brügner2018] Brügner, O.; Walter, M. Phys. Rev. Materials **2** (2018) 113603
